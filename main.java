import java.util.Arrays;
import java.util.ArrayList;

class Task1 {


    public static char[] selectionSort(char[] array) {

        for (int i = 1; i < array.length - 1; i++) {
            int index = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[index]) {
                    index = j;
                }
                char temp = array[index];
                array[index] = array[i];
                array[i] = temp;
            }
        }

        return array;
    }

    public static char[] bubbleSort(char[] array) {

        for (int i = 1; i < array.length; i++) {
            for (int j = 0; j < array.length - i; j++) {
                if (array[j] > array[j + 1]) {
                    char temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }

        return array;
    }


    public static char[] insertionSort(char[] array) {

        for (int i = 1; i < array.length; i++) {
            int j = i - 1;
            char temp = array[i];
            while (j >= 0 && array[j] > temp) {
                array[j + 1] = array[j];
                j--;
            }
            array[j + 1] = temp;
        }

        return array;
    }
}



class Task2 {
    public static String[] selectionSort(String[] array) {

        for (int i = 0; i < array.length - 1; i++) {
            int index = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j].compareTo(array[index]) < 0) {
                    index = j;
                }
                String temp = array[index];
                array[index] = array[i];
                array[i] = temp;
            }
        }

        return array;
    }

    public static String[] bubbleSort(String[] array) {

        for (int i = 1; i < array.length; i++) {
            for (int j = 0; j < array.length - i; j++) {
                if (array[j].compareTo(array[j + 1]) > 0) {
                    String temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }

        return array;
    }


    public static String[] insertionSort(String[] array) {

        for (int i = 1; i < array.length; i++) {
            int j = i - 1;
            String temp = array[i];
            while (j >= 0 && array[j].compareTo(temp) > 0) {
                array[j + 1] = array[j];
                j--;
            }
            array[j + 1] = temp;
        }

        return array;
    }


}


public class main{
    public static void main(String[] args) {


        //checking Task 1 sorting methods
        System.out.println("Task 1:");
        char[] arrayChar = new char[]{'2', 'a', '1', 'd', '$', '0', 'j', ':', '&', '}', '^', 'A', ','};

        printSortedChar(Task1.bubbleSort(arrayChar));
        System.out.println(" - This array is bubble-sorted");

        printSortedChar(Task1.insertionSort(arrayChar));
        System.out.println(" - This array is insertion-sorted");

        printSortedChar(Task1.selectionSort(arrayChar));
        System.out.println(" - This array is selection-sorted\n");


        //checking Task 2 sorting methods
        System.out.println("Task 2:");
        String[] arrayString = new String[]{"aaaaa", "aaaaab", "123", "#%;", "xyzabc", "#%a", "#%12"};

        printSortedString(Task2.bubbleSort(arrayString));
        System.out.println(" - This array is bubble-sorted");

        printSortedString(Task2.insertionSort(arrayString));
        System.out.println(" - This array is insertion-sorted");

        printSortedString(Task2.selectionSort(arrayString));
        System.out.println(" - This array is selection-sorted");
    }


    public static void printSortedChar(char[] array){
        for(int i = 0; i < array.length; i++){
            System.out.print(array[i] + " ");
        }
    }

    public static void printSortedString(String[] array){
        for(int i = 0; i < array.length; i++){
            System.out.print(array[i] + " ");
        }
    }
}
